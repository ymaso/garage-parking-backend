## Garage Parking Backend
This is the backend for the garage parking system. 

The deployment is live. 

- Production deployment - https://violent-jail-production.up.railway.app
- Test deployment - https://violent-jail-test.up.railway.app

How the system works.

- Events are updates to the status of a car in garage. 
- Every time a car enter/exits the garage/parking there are events generated. 
- So, in an ideal scenario in total there are `4` events generated. 
- These events are basically calls to the database access API with the updated status of the cars.
- When an API call is made for any endpoint, the values are first validated by the swagger schema validater.
- On successful validation, the request is submitted to the server and the server does an `insert` or `update`.
- A trigger validates the relation of the inputs. So a car can not be both `in_parking` = `true` and have a `spaceid` = `NULL`. Or a car cannot be `in_garage` = `false` and still have a `level` and `spaceid` value of not `NULL`. And so on.
- The trigger also logs the status of the car into the `logging.events_log` table.
- When a car exits parking and exits the garage, the car is removed from the table. Only the logs remain.
- If there are no other cars with the same phone number, the phone number is also removed.
- If there are no other cars with the same color, the color is also removed.



## Description
The routes available are 
```
└── / (GET)
    ├── docs (GET)
    │
    ├── events-log (GET)
    │   └── / (GET)
    │       └── :plate (GET)
    │    
    ├── garage (GET)
    │   └── / (GET)
    │       └── :plate (GET)
    │           :plate (PUT)
    │    
    └── user/:level/:spaceid (PUT)
```
`/` redirects to `/docs`.

`/docs` is the swagger testing endpoint.

`/events-log` is the endpoint for garage events. 
It contains the log of all events generated.

`/garage` is the endpoint for cars in garage.
It contains information of all cars currently present in garage.

## Branches
There are two branches. `main` and `latest`.
- `main` is the stable branch. production deployments are from the `main` branch.
- `latest` is the development branch. test deploments are from the `latest` branch.

Also, for every stable release there will be a tag generated.

## Installation

To run locally, 

- Download the repo.
- Run `yarn install`.
- Update the `process.env.DATABASE_URL` in [`index.js`](/index.js) with your Postgres connection string. The postgres user must have permission to create schemas.
- Run the create commands for domains, tables and triggers from the [`commands.sql`](/commands.sql) file.
- Run `yarn start` and the project should start working locally.

## Usage
Open `localhost:5000` in browser. You will see the swagger API testing page.

There is also a [`commands.sql`](/commands.sql) file in this project that you can refer for the test insert commands.

The database used is postgres 13.x .

## Deployment
Currently the project is deployed to [railway.app](https://railway.app).

## Roadmap
- [x] Understand requirements
- [x] Build prototype server
- [x] Integrate with database (postgres)
- [x] Add Swagger for testing
- [x] Validate all API schema with swagger
- [x] Stable release
- [x] Deploy the project live
- [x] Refactor code with proper project layout
- [x] Remove debug logs from code
- [ ] Unit Testing
- [ ] ... ?

## Authors
Yahyaa Masood

## License
GNU AGPLv3

<br>
<br>
<br>

# Things I didn't remove from default readme yet

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:33d3c1b5f3f89aa9c184abbeeb783275?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:33d3c1b5f3f89aa9c184abbeeb783275?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:33d3c1b5f3f89aa9c184abbeeb783275?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:33d3c1b5f3f89aa9c184abbeeb783275?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:33d3c1b5f3f89aa9c184abbeeb783275?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:33d3c1b5f3f89aa9c184abbeeb783275?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:33d3c1b5f3f89aa9c184abbeeb783275?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:33d3c1b5f3f89aa9c184abbeeb783275?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:33d3c1b5f3f89aa9c184abbeeb783275?https://docs.gitlab.com/ee/user/clusters/agent/)

***

