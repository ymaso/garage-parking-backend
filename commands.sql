-- Create DOMAINS
-- Enforce NOT NULL in columns that can never be NULL
CREATE DOMAIN "LEVEL" AS SMALLINT CHECK (VALUE > -1 AND VALUE < 4);
CREATE DOMAIN "SPACEID" AS SMALLINT CHECK (VALUE > -1 AND VALUE < 10);
CREATE DOMAIN "PHONE" AS VARCHAR(10) CHECK (VALUE ~ '^[0-9]{10}$');

CREATE DOMAIN "PLATE" AS VARCHAR(7) NOT NULL CHECK (VALUE ~ '^[A-Za-z0-9]{7}$');
CREATE DOMAIN "SIZE" AS VARCHAR(6) NOT NULL CHECK (VALUE IN ('small', 'medium', 'large'));
CREATE DOMAIN "COLOR" AS VARCHAR(7) NOT NULL CHECK (VALUE ~ '^#[a-fA-F0-9]{6}$');

-- Create SCHEMA
CREATE SCHEMA IF NOT EXISTS garage;
CREATE SCHEMA IF NOT EXISTS logging;

-- Set search path
SET search_path TO garage, logging, public;

-- Create table user_identifier
CREATE TABLE garage.user_identifier (
id INTEGER GENERATED ALWAYS AS IDENTITY,
phone "PHONE" UNIQUE,
PRIMARY KEY (id)
);

-- Create table vehicle_color
CREATE TABLE garage.vehicle_color (
id INTEGER GENERATED ALWAYS AS IDENTITY,
color "COLOR" UNIQUE,
PRIMARY KEY (id)
);

-- Create table vehicle_information
-- Stores all the information about vehicles currently inside garage 
CREATE TABLE garage.vehicle_information (
id INTEGER GENERATED ALWAYS AS IDENTITY,
license_plate "PLATE" UNIQUE, 
color "COLOR" REFERENCES vehicle_color(color) ON DELETE CASCADE,
size "SIZE",
in_garage bool NOT NULL, 
in_parking bool NOT NULL,
level "LEVEL",
spaceid "SPACEID",
phone "PHONE" REFERENCES user_identifier(phone) ON DELETE CASCADE,
created_at TIMESTAMPTZ DEFAULT (NOW() AT TIME ZONE 'utc'),
updated_at TIMESTAMPTZ DEFAULT (NOW() AT TIME ZONE 'utc'),
PRIMARY KEY (id),
UNIQUE(license_plate, level, spaceid)
);


-- Create table garage_events_log
-- Store log of all events
CREATE TABLE logging.events_log (
id INTEGER GENERATED ALWAYS AS IDENTITY,
license_plate "PLATE",
color "COLOR",
size "SIZE",
in_garage bool NOT NULL, 
in_parking bool NOT NULL,
level "LEVEL", 
spaceid "SPACEID",
phone "PHONE",
event_time TIMESTAMPTZ DEFAULT (NOW() AT TIME ZONE 'utc'),
PRIMARY KEY (id)
);

-- Function to update date and validate input in certain situations
CREATE OR REPLACE FUNCTION tr_update_status()
RETURNS TRIGGER AS $$
BEGIN
    -- Update the time for `updated_at` field
    NEW.updated_at = NOW() AT TIME ZONE 'utc';
    
    -- If car not in garage
    IF (NEW.in_garage = false) THEN
        -- Make sure in_parking is false and level, spacid are set to NULL
        NEW.in_parking = false;
        NEW.level = NULL;
        NEW.spaceid = NULL;
    
    -- If car not parked
    ELSIF (NEW.in_parking = false) THEN
        -- Make sure spaceid is set to NULL
        NEW.spaceid = NULL;
    END IF;

    -- If level is NULL, car is not present in garage
    IF (NEW.level = NULL) THEN
        -- Make sure in_garage and in_parking bools are set to false and spaceid is NULL
        NEW.in_garage = false;
        NEW.in_parking = false;
        NEW.spaceid = NULL;
    ELSIF (NEW.spaceid = NULL) THEN
        -- Make sure in_parking is false
        NEW.in_parking = false;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Trigger update_status
CREATE TRIGGER update_status
BEFORE INSERT OR UPDATE ON garage.vehicle_information
FOR EACH ROW
EXECUTE FUNCTION tr_update_status();

-- Function to log information in the events table and
-- remove information from vehicle_information table when a car exits the garage
CREATE OR REPLACE FUNCTION tr_update_log()
RETURNS TRIGGER AS $$
BEGIN
    -- If in parking, log only when phone number is submitted
    IF (NEW.in_parking = 'True') THEN
        IF (NEW.phone IS NOT NULL) THEN
            INSERT INTO logging.events_log
                (license_plate, color, size, in_garage, in_parking, level, spaceid, phone, event_time)
                VALUES
                (NEW.license_plate, NEW.color, NEW.size, NEW.in_garage, NEW.in_parking, NEW.level, NEW.spaceid, NEW.phone, NEW.updated_at);
        END IF;
    ELSE
        -- Log on all other event updates
        INSERT INTO logging.events_log
            (license_plate, color, size, in_garage, in_parking, level, spaceid, phone, event_time)
            VALUES
            (NEW.license_plate, NEW.color, NEW.size, NEW.in_garage, NEW.in_parking, NEW.level, NEW.spaceid, NEW.phone, NEW.updated_at);

    END IF;
    -- DEBUG ONLY
    -- RAISE NOTICE 'Test values: % % % % % % % % %', NEW.license_plate, NEW.color, NEW.size, NEW.in_garage, NEW.in_parking, NEW.level, NEW.spaceid, NEW.phone, NEW.updated_at;
    
    -- DELETE rows if car is not in garage anymore
    IF (NEW.in_garage = false AND NEW.in_parking = false) THEN
        -- Check if no cars exist with same phone
        IF NOT EXISTS (SELECT 1 FROM garage.vehicle_information WHERE license_plate != NEW.license_plate AND phone = NEW.phone LIMIT 1) THEN
            DELETE FROM garage.user_identifier where phone = NEW.phone;
        END IF;

        -- Check if other cars exists with same color
        IF NOT EXISTS (SELECT 1 FROM garage.vehicle_information WHERE license_plate != NEW.license_plate AND color = NEW.color LIMIT 1) THEN
            DELETE FROM garage.vehicle_color where color = NEW.color;
        END IF;

        -- Delete row now if it still persists after removing phone and color fk's
        DELETE FROM garage.vehicle_information where license_plate = NEW.license_plate;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

-- Trigger update_log
CREATE TRIGGER update_log
AFTER INSERT OR UPDATE ON garage.vehicle_information
FOR EACH ROW
EXECUTE FUNCTION tr_update_log();

-------------------------------------------------------
---- Test to check if database is working properly ----
---------- Test two cars with same phone --------------
-------------------------------------------------------

-- First insert color
INSERT INTO garage.vehicle_color (color) VALUES ('#012ABC') ON CONFLICT (color) DO NOTHING;

-- Test car entering into garage event
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('Z1T89C7', '#012ABC', 'small', 'True', 'False', 3, NULL)
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('F1T89C7', '#012ABC', 'large', 'True', 'False', 1, NULL)
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;

-- Test car parked event
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('Z1T89C7', '#012ABC', 'small', 'True', 'True', 3, 5)
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('F1T89C7', '#012ABC', 'large', 'True', 'True', 1, 9)
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;

-- User submits phone number. Insert into user_identifier fisrt
INSERT INTO garage.user_identifier (phone) values ('1234567890') ON CONFLICT (phone) DO NOTHING;

-- Update status, add phone in the vehicle_information table
UPDATE garage.vehicle_information set phone = '1234567890' where level = 3 and spaceid = 5;
UPDATE garage.vehicle_information set phone = '1234567890' where level = 1 and spaceid = 9;

-- Car exits parking event
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('Z1T89C7', '#012ABC', 'small', 'True', 'False', 3, NULL)
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('F1T89C7', '#012ABC', 'large', 'True', 'False', 1, NULL)
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;

-- Car exits garage event
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('Z1T89C7', '#012ABC', 'small', 'False', 'False', 0, 0) -- Should be NULL, NUll. 0,0 is used to test the trigger
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('F1T89C7', '#012ABC', 'large', 'False', 'False', NULL, NULL)
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;

-- After executing above queries there should be no rows in the test database, only logging.events

----------------------------------------------------------
---- Test to check if database is working properly x2 ----
----------------------------------------------------------

-- First insert color
INSERT INTO garage.vehicle_color (color) VALUES ('#FED987');

-- Test car entering into garage event
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('A1B23C7', '#FED987', 'large', 'True', 'False', 0, NULL);

-- Test UPSERT: car entering into garage event
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('A1B23C7', '#FED987', 'large', 'True', 'False', 0, NULL)
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;

-- Test car parked event
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('A1B23C7', '#FED987', 'large', 'True', 'False', 1, 9)
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;


-- User submits phone number. Insert into user_identifier first
INSERT INTO garage.user_identifier (phone) VALUES ('9090999909');

-- Update status, add phone in the vehicle_information table
UPDATE garage.vehicle_information set phone = '9090999909' where level = 1 and spaceid = 9;

-- Car exits parking event
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('A1B23C7', '#FED987', 'large', 'True', 'False', 1, NULL)
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;

-- Car exits garage event
INSERT INTO garage.vehicle_information 
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES 
    ('A1B23C7', '#FED987', 'large', 'False', 'False', NULL, NULL)
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
               level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;


-- Drop all created schemas
drop schema garage cascade;
drop schema logging cascade;

-- Drop all created tables
DROP TABLE logging.events_log cascade;
DROP TABLE garage.user_identifier cascade;
DROP TABLE garage.vehicle_color cascade;
DROP TABLE garage.vehicle_information cascade;


-- SELECT statements
SELECT * FROM garage.vehicle_information;
SELECT * FROM logging.events_log;

-- WITH INSERT UPDATE format for PUT request
WITH subquery as (INSERT INTO garage.user_identifier (phone) values ('7234567890') ON CONFLICT (phone) DO NOTHING returning phone)
UPDATE garage.vehicle_information set phone = subquery.phone  from subquery where level = 3 and spaceid = 5;

-- Testing a chained query for garage put methof
WITH subquery as (INSERT INTO garage.vehicle_color (color) values ('#012ABC') ON CONFLICT (color) DO NOTHING returning color)
    INSERT INTO garage.vehicle_information
    (license_plate, color, size, in_garage, in_parking, level, spaceid) 
    VALUES
    ('F1T89C7', subquery.color, 'large', 'False', 'True', 0, NULL)
    FROM subquery
    ON CONFLICT (license_plate) DO
    UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
                level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;