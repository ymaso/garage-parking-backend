'use strict'

const path = require('path')
const AutoLoad = require('fastify-autoload')

module.exports = async function (fastify, opts) {

  fastify.register(require("fastify-swagger"), {
    exposeRoute: true,
    routePrefix: "/docs",
    swagger: {
      info: {
        title: "Garage Parking Backend",
        description: "Backend for garage parking system.",
        version: "1.2.0",
      },
      tags: [
        { name: "docs", description: "Docs endpoint" },
        { name: "user", description: "User related endpoint" },
        { name: "garage", description: "Garage related endpoint" },
        { name: "log", description: "Logged events related endpoint" },
      ],
    },
  });

  fastify.register(require("fastify-postgres"), {
    connectionString: process.env.DATABASE_URL,
  });

  fastify.register(require("fastify-sensible"), {
    errorHandler: false,
  });

  // Commented out because I do not have any plugins present currently
  
  // fastify.register(AutoLoad, {
  //   dir: path.join(__dirname, 'plugins'),
  //   options: Object.assign({}, opts)
  // })

  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'routes'),
    options: Object.assign({}, opts)
  })
}
