async function getEventsLog(fastify, options, done) {
  const getItems = (req, reply) => {
    const { level, spaceid } = req.params;
    const { phone } = req.body;
    delete req.params.level, req.params.spaceid, req.body.phone;

    fastify.pg.query(
      `WITH subquery as (INSERT INTO garage.user_identifier (phone) values ('${phone}') ON CONFLICT (phone) DO NOTHING RETURNING phone)
  UPDATE garage.vehicle_information set phone = subquery.phone  from subquery where level = ${level} and spaceid = ${spaceid};`,
      (err, result) => {
        if (err) reply.send({ message: err });
        else reply.code(204).send("No Content");
      }
    );
  };

  const getItemsOpts = {
    schema: {
      description: "Update user phone number",
      tags: ["user"],
      required: ["level", "spaceid"],
      params: {
        type: "object",
        properties: {
          level: { type: "string", pattern: "^[0-3]{1}$" },
          spaceid: { type: "string", pattern: "^[0-9]{1}$" },
        },
      },
      body: {
        required: ["phone"],
        type: "object",
        properties: {
          phone: { type: "string", pattern: "^[0-9]{10}$" },
        },
      },
      response: {
        204: { type: "string" },
        400: {
          type: "object",
          properties: {
            message: { type: "object" },
          },
        },
      },
    },
    handler: getItems,
  };
  // Get all items
  fastify.put("/:level/:spaceid", getItemsOpts);
  done();
}

module.exports = getEventsLog;
