async function getGarage(fastify, options, done) {
  const getAllVehicles = (req, reply) => {
    fastify.pg.query(
      "SELECT * FROM garage.vehicle_information;",
      (err, result) => {
        if (err) reply.send({ message: err });
        else reply.send(result.rows);
      }
    );
  };
  const getVehicle = (req, reply) => {
    const { plate } = req.params;
    delete req.params.plate;

    fastify.pg.query(
      `SELECT * FROM garage.vehicle_information where license_plate = '${plate}';`,
      (err, result) => {
        if (err) reply.send({ message: err });
        else reply.send(result.rows);
      }
    );
  };

  const putVehicle = (req, reply) => {
    const { plate } = req.params;
    delete req.params.plate;

    const { color, size, in_garage, in_parking, level, spaceid } = req.body;

    let exists;

    fastify.pg.query(
      `SELECT EXISTS (SELECT * from garage.vehicle_information where license_plate = '${plate}');`,
      (err, result) => {
        if (err) reply.send({ message: err });
        else exists = result.rows[0].exists;
      }
    );

    fastify.pg.query(
      `INSERT INTO garage.vehicle_color (color) values ('${color}') ON CONFLICT (color) DO NOTHING;
          INSERT INTO garage.vehicle_information
          (license_plate, color, size, in_garage, in_parking, level, spaceid)
          VALUES
          ('${plate}', '${color}', '${size}', '${in_garage}', '${in_parking}', ${level}, ${spaceid})
          ON CONFLICT (license_plate) DO
          UPDATE SET in_garage = EXCLUDED.in_garage, in_parking = EXCLUDED.in_parking,
                     level = EXCLUDED.level, spaceid = EXCLUDED.spaceid;`,
      (err, result) => {
        if (err) reply.send({ message: err });
        // Does not exists and insert success
        else if (!exists && result) reply.code(201).send("Created");
        // Exists and just updated
        else reply.code(204).send("No Content");
      }
    );
  };

  const Item = {
    type: "object",
    properties: {
      id: { type: "number" },
      license_plate: { type: "string" },
      color: { type: "string" },
      size: { type: "string" },
      in_garage: { type: "boolean" },
      in_parking: { type: "boolean" },
      level: { type: "string" },
      spaceid: { type: "string" },
      phone: { type: "string" },
      created_at: { type: "string" },
      updated_at: { type: "string" },
    },
  };

  const schemaGetAllVehicles = {
    schema: {
      description: "Get all vehicles present in garage.",
      tags: ["garage"],
      response: {
        200: {
          type: "array",
          items: Item,
        },
        400: {
          type: "object",
          properties: {
            message: { type: "object" },
          },
        },
      },
    },
    handler: getAllVehicles,
  };

  const schemaGetVehicle = {
    schema: {
      description: "Get a vehicle present in garage using it's plate.",
      tags: ["garage"],
      params: {
        required: ["plate"],
        type: "object",
        properties: {
          plate: { type: "string", pattern: "^[A-Za-z0-9]{7}$" },
        },
      },
      response: {
        200: { type: "array", items: Item },
        400: {
          type: "object",
          properties: {
            message: { type: "object" },
          },
        },
      },
    },
    handler: getVehicle,
  };

  const schemaPutVehicle = {
    schema: {
      description: `Update information of a car present in garage. Returns \`204 No Content\` .
                    OR
                    Insert information for a new car. Returns \`201 Created\` .`,
      tags: ["garage"],
      params: {
        required: ["plate"],
        type: "object",
        properties: {
          plate: { type: "string", pattern: "^[A-Za-z0-9]{7}$" },
        },
      },
      body: {
        required: [
          "color",
          "size",
          "in_garage",
          "in_parking",
          "level",
          "spaceid",
        ],
        type: "object",
        properties: {
          color: { type: "string", pattern: "^#[A-Fa-f0-9]{6}$" },
          size: { type: "string", pattern: "^(small|medium|large){1}$" },
          in_garage: { type: "boolean" },
          in_parking: { type: "boolean" },
          level: { type: "string", pattern: "^([0-3]{1}|NULL|null){1}$" },
          spaceid: { type: "string", pattern: "^([0-9]{1}|NULL|null){1}$" },
        },
      },
      response: {
        201: { type: "string" },
        204: { type: "string" },
        400: {
          type: "object",
          properties: {
            message: { type: "object" },
          },
        },
      },
    },
    handler: putVehicle,
  };

  // Get all items
  fastify.get("/", schemaGetAllVehicles);

  // Get one item
  fastify.get("/:plate", schemaGetVehicle);

  // Put one item
  fastify.put("/:plate", schemaPutVehicle);

  done();
}

module.exports = getGarage;
