async function def(fastify, opts, done) {
    const redirect = (req, reply) => {
      reply.redirect(302, "/docs");
    };
  
    const schemaDef = {
      schema: { description: "Redirect to docs page", tags: ['docs']},
      handler: redirect,
    };
    fastify.get("/", schemaDef);
    done();
  }
  
  module.exports = def;
  