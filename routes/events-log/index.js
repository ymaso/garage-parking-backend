"use strict";

async function getEventsLog(fastify, options, done) {
  const getVehicleEvents = (req, reply) => {
    const { plate } = req.params;
    delete req.params.plate;

    fastify.pg.query(
      `SELECT * FROM logging.events_log where license_plate = '${plate}' ;`,
      (err, result) => {
        if (err) reply.send({ message: err });
        else reply.send(result.rows);
      }
    );
  };

  const getAllVehcileEvents = (req, reply) => {
    fastify.pg.query("SELECT * FROM logging.events_log;", (err, result) => {
      if (err) reply.send(err);
      else reply.send(result.rows);
    });
  };

  const Item = {
    type: "object",
    properties: {
      id: { type: "string" },
      license_plate: { type: "string" },
      color: { type: "string" },
      size: { type: "string" },
      in_garage: { type: "boolean" },
      in_parking: { type: "boolean" },
      level: { type: "string" },
      spaceid: { type: "string" },
      phone: { type: "string" },
      event_time: { type: "string" },
    },
  };

  const schemaVehicleEvents = {
    schema: {
      description: "Get all logged events for a particular vehicle.",
      tags: ["log"],
      params: {
        required: ["plate"],
        type: "object",
        properties: {
          plate: {
            type: "string",
            pattern: "^[A-Za-z0-9]{7}$",
          },
        },
      },
      response: {
        200: {
          type: "array",
          items: Item,
        },
        500: {
          type: "object",
        },
      },
    },
    handler: getVehicleEvents,
  };

  const schemaAllVehicleEvents = {
    schema: {
      description: "Get all logged events for all vehicles.",
      tags: ["log"],
      response: {
        200: {
          type: "array",
          items: Item,
        },
        500: {
          type: "object",
        },
      },
    },
    handler: getAllVehcileEvents,
  };
  // Get all items
  fastify.get("/:plate", schemaVehicleEvents);
  fastify.get("/", schemaAllVehicleEvents);
  done();
}

module.exports = getEventsLog;
